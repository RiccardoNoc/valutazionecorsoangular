import { StudenteService } from './../services/studente.service';
import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http'
import * as $ from 'jquery';

@Component({
  selector: 'app-studente',
  templateUrl: './studente.component.html',
  styleUrls: ['./studente.component.css']
})
export class StudenteComponent implements OnInit {

  constructor(private studenteService: StudenteService) { }
  corso = -1;
  modulo = -1;
  docente = "";
  fineQuestionario: boolean = false;
  corsi: any;
  moduli: any;
  readonly ROOT_URL = "http://localhost:8081/ProvaProgettoValutazione/rest/domande";

  ngOnInit() {
    this.caricaCorsi();
  }

  caricaCorsi(){
    this.studenteService.getCorsi().subscribe((data: any) => { this.corsi = data })
  }

  caricaModuli() {
    this.studenteService.getModuli().subscribe((data: any) => { this.moduli = data })
  }

  caricaGiudizio() {    //aggiunge la valutazione completa al DB tramite servizio REST
    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    var body =
      'corso=' + $("#corso").val() + '&modulo=' + $("#modulo").val() + '&docente=' + $("#docente").val() + '&valutazione=' + $("#valutazione").val() +
      '&complessivo=' + $("#complessivo").val() + '&nome=' + $("#nome").val() + '&analitico1' + $("input[name='analitico1']:checked").val()
      + '&analitico2' + $("input[name='analitico2']:checked").val() + '&analitico3' + $("input[name='analitico3']:checked").val()
      + '&analitico4' + $("input[name='analitico4']:checked").val() + '&analitico5' + $("input[name='analitico5']:checked").val() +
      '&analitico6' + $("input[name='analitico6']:checked").val()
      const obs = {
        next: x => {},
        error: err => alert('Si è verificato un problema. Riprovare.'),
        complete: () =>  this.fineQuestionario = true
      };
    this.studenteService.caricaGiudizio(myheader,body).subscribe(obs)
  }
}
