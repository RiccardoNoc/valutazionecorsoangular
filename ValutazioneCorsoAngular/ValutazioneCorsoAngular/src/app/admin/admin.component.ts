import { AdminService } from './../services/admin.service';
import { PdfComponent } from './../pdf/pdf.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http'
import { faTrashAlt, faCheck, faSearch, faTrashRestore, faFilePdf } from '@fortawesome/free-solid-svg-icons';
import { Giudizio } from '../models/giudizio.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  @ViewChild(PdfComponent) pdf: PdfComponent;
  oggettoGiudizio: Giudizio;
  selectCerca: string;
  giudizi: any;
  cerca: string;
  showPdf: boolean = false;
  //icone:
  faTrashAlt = faTrashAlt;
  faCheck = faCheck;
  faSearch = faSearch;
  faFilePdf = faFilePdf;
  faTrashRestore = faTrashRestore;
  readonly ROOT_URL = "http://localhost:8081/ProvaProgettoValutazione/rest/amministratore";
  constructor(private adminService: AdminService) { }

  lista = 'attivo'

  ngOnInit() {
    this.caricaGiudiziAttivi()
  }

  caricaGiudiziAttivi() {
    this.adminService.getGiudiziAttivi().subscribe((data: any) => { this.giudizi = data })
  }

  caricaTuttiGiudizi() {
    this.lista = 'tutto';
    this.adminService.getTuttiGiudizi().subscribe((data: any) => { this.giudizi = data })
  }

  cancellaGiudizio(id: number) {
    var lista : string = this.lista
    this.adminService.deleteGiudizio(id).subscribe((data: any) => {
      if (lista == 'attivo') {
        this.ricaricaLista();
      }
      else 
        this.caricaTuttiGiudizi();  
    })
  }

  attivaGiudizio(id: number) {
    this.adminService.restoreGiudizio(id)
    .subscribe((data: any) => {
      if (this.lista == 'attivo') {
        this.ricaricaLista();
      }
      else {
        this.caricaTuttiGiudizi();
      }
    })
  }

  ricaricaLista() {
    this.lista = 'attivo'
    this.adminService.getGiudiziAttivi().subscribe((data:any)=>{this.giudizi=data})
  }

  stampaPdf(giudizio: Giudizio) {
    this.oggettoGiudizio = giudizio;
    this.showPdf = true;
    if (this.showPdf == true) {
      this.funzione2();
    }
  }

  funzione2() {
    this.pdf.settaCheck();
  }
}
