
import { PipeTransform, Pipe} from '@angular/core';

@Pipe({
name: 'filtroTutto'
})
export class FiltroTuttoPipe implements PipeTransform{
    transform(valori: any, cerca: any) {
if(!valori || !cerca){
return valori;
}

return valori.filter((data)=>this.controlla(data,cerca))
    }

    controlla(data, vals) {
        return Object.keys(data).map((key) => {
            return new RegExp(vals, 'gi').test(data[key]);
        }).some(result => result);
      }
}