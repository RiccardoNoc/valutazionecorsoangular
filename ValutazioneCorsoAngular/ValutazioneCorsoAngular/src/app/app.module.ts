import { FormsModule } from '@angular/forms';
import { AdminComponent } from './admin/admin.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { StudenteComponent } from './studente/studente.component';
import { NavComponent } from './nav/nav.component';
import { CorsiComponent } from './corsi/corsi.component';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FiltroTuttoPipe } from './admin/filtro-tutto.pipe';
import { LoginComponent } from './login/login.component';
import { RoutesGuardService } from './services/routes-guard.service';
import { PdfComponent } from './pdf/pdf.component';


const routes : Routes = [
{
  path:'login/studente',
  pathMatch: 'full',
  redirectTo: 'studente'
},
{
  path:'studente',
  component: StudenteComponent
},
{
  path:'',  //il primo component che viene generato
  pathMatch:'full',
  redirectTo: 'login'
},
{
  path:'admin',
  component: AdminComponent,

  //Questa linea impedisce l'accesso alla rotta Admin se non si effettua il login
  canActivate : [RoutesGuardService]
},
{
  path:'corsi',
  component: CorsiComponent
},
{
  path:'login',
  component: LoginComponent
}
];

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    StudenteComponent,
    NavComponent,
    CorsiComponent,
    FiltroTuttoPipe,
    LoginComponent,
    PdfComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent,AdminComponent,StudenteComponent,LoginComponent]
})
export class AppModule { }
