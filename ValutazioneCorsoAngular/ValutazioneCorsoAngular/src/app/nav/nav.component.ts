import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { faUser } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public adminLogato;
  faUser=faUser;

  constructor(private auth : AuthService, private router : Router) {

   }



  ngOnInit(): void {

    this.auth.isAdmin.subscribe( value => {
      this.adminLogato = value;

    });

  }



  logout(){

    this.auth.logout();
   
  }


  lista='attivo'
  caricaDaRoute() {
    $('#risposte').empty()
          $.getJSON("http://localhost:8081/ProvaProgettoValutazione/rest/amministratore",{}, this.vediRisposte)
}


 vediRisposte(giudizi) {

   $('#risposte').append("<tr><th>Modulo</th><th>Docente</th><th>Valutazione</th><th>Giudizio complessivo</th><th>Durata del corso</th><th>Qualità della docenza</th><th>Strutture informatiche HW</th><th>Strumenti informatici SW</th><th>Qualità della documentazione</th><th>Livello di conoscenza acquisita</th><th>Allievo</th><th>Data</th><th>Validità</th><th>Opzioni</th></tr>");
    $.each(giudizi, 
      function(idx,giudizio) 
      {
         $("#risposte").append("<tr><td>" + giudizio.modulo + "</td><td>" + giudizio.docente + "</td><td>" +  giudizio.valutazione + "</td><td>" + giudizio.complessivo + "</td><td>" + giudizio.durataCorso + "</td><td>" +giudizio.qualitaDocenza + "</td><td>" +giudizio.struttureHW + "</td><td>" +giudizio.strumentiSW + "</td><td>" +giudizio.qualitaDocumentazione + "</td><td>" +giudizio.livelloAcquisito + "</td><td>" +giudizio.nome + "</td><td>" +giudizio.data +"</td><td>" +giudizio.valore + "</td><td><div class=\"container\" id=\"bottone"+giudizio.id+"\">");
         if (giudizio.valore=='attivo') {
         $("#bottone"+giudizio.id).html("<button class=\"btn btn-danger\" (click)=\"cancella("+giudizio.id+")\" >Cancella </button></td></tr>") }
         else {
         $("#bottone"+giudizio.id).html("<button class=\"btn btn-success\" (click)=\"attiva("+giudizio.id+")\">Riattiva </button></td></tr>") };
      }
    );
 }


 vediTutto() {

  this.lista='tutto';
$('#risposte').empty();
$.getJSON("http://localhost:8081/ProvaProgettoValutazione/rest/amministratore/tutto",{}, this.vediRisposte);

}


cancella(id : number) {
  $.ajax( 
    {
      url : "http://localhost:8081/ProvaProgettoValutazione/rest/amministratore/" + id,
        method : "delete",
      complete : this.carica
    }
   );
}


carica() {
if (this.lista=='attivo') {
   this.ricaricaLista();
   }
else {
    this.vediTutto();
     }
}


attiva(id) {
  $.ajax( 
    {
      url : "http://localhost:8081/ProvaProgettoValutazione/rest/amministratore/attiva/" + id,
        method : "put",
      complete : this.vediTutto
    }
   );
}


ricaricaLista() {

 this.lista='attivo'
$('#risposte').empty();
$.getJSON("http://localhost:8081/ProvaProgettoValutazione/rest/amministratore",{}, this.vediRisposte);

}


getCorsi(){
  $.ajax({
  method:'get',
  url:'http://localhost:8081/ProvaProgettoValutazione/rest/modulo/corsi'
  }
  );
  }
}


