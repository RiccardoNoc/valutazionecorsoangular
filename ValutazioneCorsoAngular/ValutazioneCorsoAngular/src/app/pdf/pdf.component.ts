import { Giudizio } from './../models/giudizio.model';
import { Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';
import * as jsPDF from 'jspdf'
import * as $ from 'jquery'
import * as html2canvas from 'html2canvas'
import { stringify } from 'querystring';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit  {
  @ViewChild('content', { 'static': true }) content:ElementRef;
  @Input() model : Giudizio;
  title = 'stampaPDF';

  ngOnInit() {
    $("#comp"+this.model.complessivo).prop("selected","selected");
    $("#durata" + this.model.durataCorso).prop("checked","checked");
    $("#docenza"+this.model.qualitaDocenza).prop("checked","checked");
    $("#hw"+this.model.struttureHW).prop("checked","checked");
    $("#sw"+this.model.strumentiSW).prop("checked","checked");
    $("#documentazione"+this.model.qualitaDocumentazione).prop("checked","checked");
    $("#conoscenza"+this.model.livelloAcquisito).prop("checked","checked");
  }

  settaCheck(){
    $("#comp"+this.model.complessivo).prop("selected","selected");
    $("#durata" + this.model.durataCorso).prop("checked","checked");
    $("#docenza"+this.model.qualitaDocenza).prop("checked","checked");
    $("#hw"+this.model.struttureHW).prop("checked","checked");
    $("#sw"+this.model.strumentiSW).prop("checked","checked");
    $("#documentazione"+this.model.qualitaDocumentazione).prop("checked","checked");
    $("#conoscenza"+this.model.livelloAcquisito).prop("checked","checked");
  }

  downloadPDF() {
    const div = document.getElementById('content');
    const options = {
      background: 'white',
      scale: 3
    };

    html2canvas(div, options).then((canvas) => {

      var img = canvas.toDataURL("image/PNG");
      var doc = new jsPDF('p','pt','a4');
      const bufferX = 5;
      const bufferY = 5;
      const imgProps = (<any>doc).getImageProperties(img);
      const pdfWidth = (doc.internal.pageSize.getWidth() - 2 * bufferX);
      const pdfHeight = ((imgProps.height * pdfWidth) / imgProps.width);
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((doc) => {
      doc.save('giudizio.pdf');  
    });   
  }
}
