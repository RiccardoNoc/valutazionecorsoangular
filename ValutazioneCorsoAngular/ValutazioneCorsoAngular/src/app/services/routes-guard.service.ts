import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class RoutesGuardService implements CanActivate {

  constructor() { }



  public canActivate(){

    if(localStorage.getItem("token")){
      return true;

    }
    return false;
  
  }
}
