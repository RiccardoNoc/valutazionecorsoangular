import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
    providedIn: 'root',
})
export class AdminService {

    constructor(private http: HttpClient) {
    }
    readonly ROOT_URL = "http://localhost:8081/ProvaProgettoValutazione/rest/amministratore";

    getGiudiziAttivi() {
        return this.http.get(this.ROOT_URL)
    }

    getTuttiGiudizi(){
        return this.http.get(this.ROOT_URL + '/tutto')
    }

    deleteGiudizio(id: number){  
        return this.http.delete(this.ROOT_URL + '/' + id)
    }

    restoreGiudizio(id: number) {
        return this.http.put((this.ROOT_URL + '/attiva/' + id), this.http.get(this.ROOT_URL))
      }




    
}