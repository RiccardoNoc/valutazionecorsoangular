import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
    providedIn: 'root',
})
export class StudenteService {

    constructor(private http: HttpClient) {
    }
    readonly ROOT_URL = "http://localhost:8081/ProvaProgettoValutazione/rest/domande";

    getCorsi() {
        return this.http.get(this.ROOT_URL + '/corsi')
    }

    getModuli() {
        return this.http.get(this.ROOT_URL + '/moduli/' + $("#corso").val())
    }

    caricaGiudizio(myheader, body) {    //aggiunge la valutazione completa al DB tramite servizio REST

        return this.http.post(this.ROOT_URL, body, { headers: myheader })
    }
}