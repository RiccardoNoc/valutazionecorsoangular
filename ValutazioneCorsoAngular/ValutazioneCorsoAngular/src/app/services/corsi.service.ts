import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
    providedIn: 'root',
})
export class CorsiService {

    constructor(private http: HttpClient) {
    }
    readonly ROOT_URL = "http://localhost:8081/ProvaProgettoValutazione/rest/modulo"; //era /corsi

    start1() {
        return this.http.get(this.ROOT_URL + '/corsi')
    }

    start2() {
        return this.http.get(this.ROOT_URL + '/moduli')
    }

    changeCorso(id: number) {
        return this.http.delete('http://localhost:8081/ProvaProgettoValutazione/rest/corso/' + id)
    }

    getModuliperId(id: number) {
        return this.http.get(this.ROOT_URL + '/moduliCorso/' + id)
    }

    addNewModulo(myheader, body) {
        return this.http.post(this.ROOT_URL + '/nuovoModulo', body, { headers: myheader })
    }

    addCorso2(myheader, body) {
        return this.http.post(this.ROOT_URL, body, { headers: myheader })
    }

    addCorso3(myheader, body) {
        return this.http.post(this.ROOT_URL + '/salvaModuli', body, { headers: myheader })
    }

    modify(myheader, body) {
        return this.http.post(this.ROOT_URL + '/salvaNuoviModuli', body, { headers: myheader })
    }
}