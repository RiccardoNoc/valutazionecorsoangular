import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private _isAdmin = new BehaviorSubject(this.tokenCheck());


  constructor() {

  }



  public get isAdmin() {
    return this._isAdmin;
  }



  public tokenCheck() {

    if (localStorage.getItem("token")) {
      return true;

    } else {
      return false;

    }

  }



  public login(email: string, password: string) {

    if (email == "admin" && password == "admin") {

      localStorage.setItem("token", email);
      this._isAdmin.next(true)
      return true;

    }

    this._isAdmin.next(false)
    return false;

  }



  public logout() {

    localStorage.removeItem("token");
    this._isAdmin.next(false)

  }


}
