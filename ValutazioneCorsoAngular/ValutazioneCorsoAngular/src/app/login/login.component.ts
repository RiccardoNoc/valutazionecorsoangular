import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private email : string;
  private password : string;
  private errorMsg : string;


  constructor( private auth : AuthService, private router: Router ) { }

  ngOnInit(): void {
  }


  login(form : NgForm){

    this.email = form.value.email;
    this.password = form.value.password;


    if(this.auth.login(this.email, this.password)){
      
      this.router.navigate(["admin"]);

    }else{  
      
      this.errorMsg = "Email o Password errati";

  }

}

getErrorMsg(){

  return this.errorMsg;

}

  
  logout(){

    this.auth.logout();
    
  }

}
