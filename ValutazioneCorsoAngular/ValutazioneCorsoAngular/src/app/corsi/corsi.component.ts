import { CorsiService } from './../services/corsi.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { faTrashAlt, faTrashRestore, faSearch, faEdit, faCheck, faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery';

@Component({
  selector: 'app-corsi',
  templateUrl: './corsi.component.html',
  styleUrls: ['./corsi.component.css']
})
export class CorsiComponent implements OnInit {
  show: boolean = false;
  showNewCorso: boolean = false;
  showNewModulo: boolean = false;
  errModuloGlobale: boolean = false;
  errCorsoGlobale: boolean = false;
  validModulo = "";
  validCorso = "";
  idCorso: number;
  corsi: any;
  indexGlobale: number = 0;
  cerca: any;
  moduli: any;
  modulo: any;
  tuttimoduli: any;
  nomecheck: any;
  //icone:
  faTrashAlt = faTrashAlt; faTrashRestore = faTrashRestore; faSearch = faSearch; faEdit = faEdit; faCheck = faCheck;
  faSyncAlt = faSyncAlt;

  readonly ROOT_URL = "http://localhost:8081/ProvaProgettoValutazione/rest/modulo"; //era /corsi

  constructor(private corsiService: CorsiService) {
  }

  ngOnInit() {
    this.corsiService.start1().subscribe((data: any) => { this.corsi = data })
    this.corsiService.start2().subscribe((data: any) => { this.tuttimoduli = data })
  }

  cambiaStatusCorso(id: number) {
    this.corsiService.changeCorso(id).subscribe((data: any) => { this.refreshTableCorsi() })
  }

  caricaModuliperId(id: number) {
    this.idCorso = id;
    console.log(id);
    this.corsiService.getModuliperId(id).subscribe((data: any) => {
      this.moduli = data;
      this.show = !this.show;
    })
  }

  modifica(idCorso) {
    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    this.corsiService.start2().subscribe((data: any) => {
      data.forEach(modulo => {
        var valore = 'check' + modulo.id + 'id' + idCorso
        var body = 'valore=' + $("input[name='" + valore + "']:checked").serialize() + '&indice=' + idCorso + '&idModulo=' + modulo.id + ''
        this.corsiService.modify(myheader, body).subscribe((data: any) => { })
      })
      this.show = false;
      alert('Moduli aggiornati');
    }
    )
  }

  checkEsistenzaModulo() {
    var err: boolean = false;
    this.corsiService.start2().subscribe((data: any) => {
      data.forEach(modulo => {
        console.log(modulo.nome)
        if ($("#nomeModulo").val().toString().toUpperCase() == modulo.nome) {
          err = true;
        }
      })
      if (err == false) {
        this.errModuloGlobale = false;
        this.aggiungiNuovoModulo()
      } else this.errModuloGlobale = true;
    })
  }

  aggiungiNuovoModulo() {
    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    let body = new HttpParams();
    body = body.set('nomeModulo', $("#nomeModulo").val().toString().toUpperCase());

    this.corsiService.addNewModulo(myheader, body).subscribe((data: any) => { this.show = false, alert('Nuovo modulo aggiunto!'), this.refreshCorsiEModuli })
  }

  checkEsistenzaCorso() {
    var err: boolean = false;
    this.corsiService.start1().subscribe((data: any) => {
      data.forEach(corso => {
        console.log(corso.nomeCorso)
        if ($("#nomeCorso").val().toString().toUpperCase() == corso.nomeCorso) {
          err = true;
        }
      }
      )
      if (err == false) {
        this.errCorsoGlobale = false;
        this.aggiungiCorso()
      } else this.errCorsoGlobale = true;
    })
  }

  aggiungiCorso() {
    this.show = false;
    this.corsiService.start1().subscribe((data: any) => { this.contaCorsi(data) })
  }

  contaCorsi(corsi) {
    var index: number = 0;
    corsi.forEach(function () {
      index = index + 1;
    });
    this.indexGlobale = index
    this.aggCorso2()
    this.aggCorso3()
  }

  aggCorso2() {
    console.log('sono in 2')
    console.log(this.indexGlobale)
    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    var body = 'nomeCorso=' + $("#nomeCorso").val().toString().toUpperCase() + '&indice=' + this.indexGlobale + ''
    this.corsiService.addCorso2(myheader, body).subscribe((data: any) => { })
  }

  aggCorso3() {
    console.log('sono in 3')
    console.log(this.indexGlobale)
    const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    this.tuttimoduli.forEach(modulo => {
      var valore = 'check' + modulo.id
      var body = 'valore=' + $("input[name='" + valore + "']:checked").serialize() + '&indice=' + this.indexGlobale + '&idModulo=' + modulo.id + ''
      this.corsiService.addCorso3(myheader, body).subscribe((data: any) => { })
    }
    )
    this.refreshCorsiEModuli()
  }

  refreshCorsiEModuli() {   //dopo un' aggiunta, ricarica corsi e moduli per proseguire il lavoro
    console.log('Refresh corsi e moduli...');
    this.corsiService.start1().subscribe((data: any) => { this.corsi = data })
    this.corsiService.start2().subscribe((data: any) => { this.tuttimoduli = data })
  }

  refreshTableCorsi() {   //refresha la tabella
    this.show = false;
    this.corsiService.start1().subscribe((data: any) => { this.corsi = data })
  }

}


