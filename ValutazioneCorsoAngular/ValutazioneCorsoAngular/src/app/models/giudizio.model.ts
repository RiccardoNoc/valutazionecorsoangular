export class Giudizio {
    id : number;
    corso: string;
    nomeCorso : string;
    modulo: string;
    nomeModulo: string;
    docente : string;
    valutazione : string;
    complessivo: number;
    durataCorso: number;
    qualitaDocenza : number;
    struttureHW : number;
    strumentiSW : number;
    qualitaDocumentazione : number;
    livelloAcquisito: number;
    nome: string;
    data : Date;
    valore: string;
}